package com.github.gitsby.db.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Kasyanov Maxim on 7/11/2017.
 */
@Repository
public interface TestDao {

    @Insert("insert into dummy values(#{name},#{surname})")
    void insertDummy(@Param("name") String name, @Param("surname") String surname);

}
