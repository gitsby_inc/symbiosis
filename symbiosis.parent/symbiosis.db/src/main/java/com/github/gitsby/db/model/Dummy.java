package com.github.gitsby.db.model;

/**
 * Created by Kasyanov Maxim on 7/11/2017.
 */
public class Dummy implements GenericModel {

    public String name;
    public String surname;

    @Override
    public String toString() {
        return "Dummy{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
