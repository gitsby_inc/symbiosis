package com.github.gitsby.db.model;

import java.io.Serializable;

/**
 * Created by Kasyanov Maxim on 7/11/2017.
 */
public interface GenericModel extends Serializable {
}
