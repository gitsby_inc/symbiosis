import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Http, RequestMethod, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class APIService {
    private token: string;
    readonly hostname: string;

    constructor(private http: Http) {
        this.hostname = environment.apiServer;
    }

    setToken(token: string) {
        this.token = token;
        localStorage.setItem('token', token);
    }

    private getAPIUrl(url: string) {
        let URL = url.startsWith('/') ? url: '/' + url; 
        return this.hostname + URL;
    }

    private getSearchString(dct: any) {
        let search: string[] = [];

        for (let param in dct) {
            let temp = [param, '=', dct[param]].join('');
            search.push(temp);
        }

        return search.join('&');
    }

    private handleError(error: any) {
        console.log('Error!!!', error);
        return Promise.reject(error.message || error);
    }

    auth(login, password): Promise<any> {
        return this.http.get(this.getAPIUrl('auth'), {
            method: RequestMethod.Post,
            search: this.getSearchString({
                login: login,
                password: password
            })
        }).toPromise().then(res => res.json())
            .catch(this.handleError);
    }
}
