import { Component, OnInit } from '@angular/core';
import { APIService } from '../shared/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  iin: string;
  password: string;
  btnActive: boolean = true;

  constructor(private apiService: APIService,
              private router: Router) {}

  ngOnInit() {
  }

  auth() {
    this.btnActive = false;

    this.apiService.auth(this.iin, this.password)
      .then(res => {
        this.apiService.setToken(res.token);
        this.router.navigateByUrl('/');
      });
  }
}
