import { Symbiosis.ClientPage } from './app.po';

describe('symbiosis.client App', () => {
  let page: Symbiosis.ClientPage;

  beforeEach(() => {
    page = new Symbiosis.ClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
