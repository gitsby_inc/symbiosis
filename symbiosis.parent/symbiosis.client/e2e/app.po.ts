import { browser, by, element } from 'protractor';

export class Symbiosis.ClientPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
