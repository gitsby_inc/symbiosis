package com.github.gitsby.server.controller.listener;

import com.github.gitsby.server.liquibase.PostgresLiquibase;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Kasyanov Maxim on 7/12/2017.
 */
public class PostgresLiquiBaseListener implements ServletContextListener {


    @Override
    public void contextDestroyed(ServletContextEvent context) {
    }

    @Override
    public void contextInitialized(ServletContextEvent context) {

        System.err.println("********************************************************");
        System.err.println("********************************************************");
        System.err.println("** HOME USER DIR: " + System.getProperty("user.home"));
        System.err.println("********************************************************");
        System.err.println("********************************************************");

        try {
            new PostgresLiquibase().apply();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}