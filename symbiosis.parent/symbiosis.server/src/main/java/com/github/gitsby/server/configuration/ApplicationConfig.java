package com.github.gitsby.server.configuration;

import com.github.gitsby.db.configuration.MyBatisConfig;
import com.github.gitsby.db.dao.DaoConfiguration;
import com.github.gitsby.db.service.ServiceConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created by Kasyanov Maxim on 6/14/2017.
 */
@Configuration
@Import({DaoConfiguration.class, ServiceConfiguration.class,
        WebConfig.class, MyBatisConfig.class})
public class ApplicationConfig {
}
