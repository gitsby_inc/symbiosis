package com.github.gitsby.server.liquibase;

import liquibase.Liquibase;
import liquibase.database.core.PostgresDatabase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Kasyanov Maxim on 7/12/2017.
 */

public class PostgresLiquibase {

    public void apply() throws Exception {
        Class.forName("org.postgresql.Driver");

        String url = "jdbc:postgresql://127.0.0.1:5432/dev";
        String username = "postgres";
        String password = "1";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PostgresDatabase postgresDatabase = new PostgresDatabase();
            postgresDatabase.setConnection(new JdbcConnection(connection));
            new Liquibase("liquibase.symbiosis/changelog-master.xml", new ClassLoaderResourceAccessor(), postgresDatabase).update("");
        }

    }
}
