package com.github.gitsby.server.controller;

import com.github.gitsby.db.dao.TestDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Kasyanov Maxim on 7/11/2017.
 */
@RestController
public class TestController {

    @Autowired
    private TestDao testDao;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity<String> test() {
        return new ResponseEntity<>("Work " + System.currentTimeMillis(), HttpStatus.OK);
    }


    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void trySave() {
        testDao.insertDummy("Maxim","Kasyanov");
    }
}
